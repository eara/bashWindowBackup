#!/bin/sh

########################################################
#
#  Use this script to save Windows users directory
#
########################################################

########################################################
#
#		Checking parameters
#
########################################################

# echo help when no parameter is given
if [ $# -eq 0 ]; then
  echo "Usage:"
  echo "windowsBackup.sh backup_machine_name windows_mount_point destination_path"
  exit 0
fi

# Archive name
if [ $# -lt 1 ]; then
  echo "missing archive name"
  exit 0
fi

# source path as parameter
if [ $# -lt 2 ]; then
  echo "missing source to be backup"
  echo "Usaly Windows mount point"
  exit 0
fi

# destination path as parameter
if [ $# -lt 3 ]; then
  echo "missing destination path"
  exit 0
fi

########################################################
#
#  		  Backup user data
#
########################################################

# Variable
cd $2"/.."
source=$2
destination=$3$(date +%F)"_"$1
browserProfiles=$destination"_browser_profile_file.txt"

tar --atime-preserve -cvf $destination"_userData.tar.gz" --exclude="*/AppData/*" --exclude="*/Users/Public/*" $source

#########################################################
#
#		Backup browsers profiles
#
#########################################################

echo "Fetching google profil"
find $sources -type d -path "*/AppData/Local/Google/Chrome" > $browserProfiles
echo "Fetching firefox profil"
find $sources -type d -path "*/AppData/Local/Mozilla/Firefox" >> $browserProfiles

tar --atime-preserve -cvf $destination"_browserProfile.tar.gz" -T $browserProfiles

echo "Done !"

# end
